//Requiring following libraries:
//needle : HTTP requests
//fs : NodeJS native File system controls
//myLib : helper library developed by me

var needle = require('needle');
var fs = require('fs');
var myLib = require('./lib/myLib.js');


//Output file (All patient-med combos)
var filename = 'tempObj.txt';

//REST Resource location for BMH
var REST_URL = 'http://54.210.77.98:80/rest/';
//Table this code is concerned with
var table_name = 'ms_medications_adherence';

//Config file
var config = fs.readFileSync('DFKeys.json');
config  = JSON.parse(config);

//Getting config
var appName = config.appName;
var email = config.email;
var password = config.password;


//HTTP header options
var options= {
				headers: {
					"X-DreamFactory-Application-Name": appName 
				},
				json : true
			};


var data = {
	
				"email":email,
				"password":password ,
				"duration":0
	
			};

//HTTP request to create session

needle.post(REST_URL+'user/session',data,options,function(err,resp,body){
	if(err){
		console.log(err);
	}
	else{
		
		//Get session id
		var session_id = body.session_id;

		//Use session id
		var userfetch_options = {
				headers: {
					"X-DreamFactory-Application-Name" : appName,
					"X-DreamFactory-Session-Token" : session_id
				},
				json:true
		}


		//Run stored proc to create view
		needle.get(REST_URL+'db/_proc/proc_uid_meds',userfetch_options,function(err,resp,body){
			if(err){
				console.log("ERROR in PROC!");
			}
		});


		//Get patient-med combos from view
		needle.get(REST_URL+'db/ms_adherence_view',userfetch_options,function(err,resp,body){
			if(err){
				console.log("ERROR in getting from view");
			}
			else{
				
				//All patient-meds combos
				var obj = body.record;
				
				//Location of final object written to output file
				var finalObj = new Array();
				
				//For each patient-med combo
				for(i in obj){
					//No of weeks to get adherence for
					var nWeeks = 10;
					//Form URL to get adherence for last nWeeks for this combo
					var reqURL = REST_URL + "db/" + table_name + "?filter=uid%3D'"+obj[i].uid+"'%20and%20medicine_name%3D'"+obj[i].medicine_name+"'&limit="+nWeeks+"&order=date%20desc&fields=uid%2Cmedicine_name%2Cdate%2Ccolor";
					
					//Get adherence from URL
					needle.get(reqURL,userfetch_options,function(err,resp,body){
						
						//Adherence object
						var innerObj = body.record;

						var testObj = {};
						testObj.uid = body.record[0].uid;
						testObj.medicine_name = body.record[0].medicine_name;
						testObj.dates = new Array();
						testObj.color = new Array();
						for(j in body.record){
							testObj.dates.push(body.record[j].date);
							testObj.color.push(body.record[j].color);
						}

						//Call helper function in lib/myLib.js to preprocess and write retrieved data
						myLib.normalizeObject(testObj,filename);
					});	
				}
			}
		});
	}
});
